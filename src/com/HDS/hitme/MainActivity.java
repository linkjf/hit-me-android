package com.HDS.hitme;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.devspark.appmsg.AppMsg;

public class MainActivity extends Activity implements OnClickListener, OnSeekBarChangeListener {

	// Variables de juego
	private int score;
	private int round;
	private int currentValue;
	private int targetValue;

	// Componentes graficos
	private Button newGameButton;
	private Button aboutButton;
	private Button hitmeButton;
	private SeekBar pointsSeekBar;
	private TextView scoreTextView;
	private TextView roundTextView;
	private TextView targetTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		newGameButton = (Button) findViewById(R.id.new_game_button);
		newGameButton.setOnClickListener(this);
		aboutButton = (Button) findViewById(R.id.about_button);
		aboutButton.setOnClickListener(this);
		hitmeButton = (Button) findViewById(R.id.hitme_button);
		hitmeButton.setOnClickListener(this);
		pointsSeekBar = (SeekBar) findViewById(R.id.points_seekbar);
		pointsSeekBar.setOnSeekBarChangeListener(this);
		pointsSeekBar.setProgress(50);
		scoreTextView = (TextView) findViewById(R.id.score_label);
		roundTextView = (TextView) findViewById(R.id.round_label);
		targetTextView = (TextView) findViewById(R.id.target_label);

		newGame();
		updateTextViews();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {

		case R.id.new_game_button:

			newGame();
			updateTextViews();

			break;
		case R.id.hitme_button:

			int difference = Math.abs(targetValue - currentValue);
			int points = 100 - difference;

			String message = "";
			if (difference == 0) {
				points += 100;
				message += "Perfect!, You scored " + (points) + " points.";
			}
			else if (difference < 5) {
				points += 50;
				message += "You almost had it!, You scored " + (points) + " points.";

			}
			else if (difference < 10) {
				message += "Pretty good!, You scored " + (points) + " points.";
			}
			else {
				message += "Not even close..., You scored " + (points) + " points.";
			}

			AppMsg.makeText(MainActivity.this, message, AppMsg.STYLE_INFO).show();

			score += points;

			newRound();
			updateTextViews();

			break;

		case R.id.about_button:

			Intent intent = new Intent(MainActivity.this, AboutActivity.class);
			startActivity(intent);
			break;
		}

	}

	void newRound() {
		round += 1;
		targetValue = 1 + new Random().nextInt(Math.abs(100 - 1));
		currentValue = 50;
		pointsSeekBar.setProgress(50);
	}

	void newGame() {
		score = 0;
		round = 0;
		newRound();
	}

	void updateTextViews() {
		targetTextView.setText(String.valueOf(targetValue));
		scoreTextView.setText(String.valueOf(score));
		roundTextView.setText(String.valueOf(round));

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

		currentValue = seekBar.getProgress();

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

}
